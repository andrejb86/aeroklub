const validator = require('validator');
const isEmpty = require ('./is-empty');

module.exports = function validatePositionInput(data) {
  let errors = {};

  data.latitude = !isEmpty(data.latitude) ? data.latitude : '';
  data.longitude = !isEmpty(data.longitude) ? data.longitude : '';
  data.speed = !isEmpty(data.speed) ? data.speed : 0;
  data.altitude = !isEmpty(data.altitude) ? data.altitude : '';
  data.heading = !isEmpty(data.heading) ? data.heading : '';
  data.uniqueident = !isEmpty(data.uniqueident) ? data.uniqueident : '';

  if (!validator.isFloat(data.latitude, { min: 0.00, max: 90.00 })) {
    errors.latitude = 'Latitude must be between 0 and 90 degrees.';
  }

  if (validator.isEmpty(data.latitude)) {
    errors.latitude = 'Latitude field is required.';
  }

  if (!validator.isFloat(data.longitude, { min: 0.00, max: 180.00 })) {
    errors.longitude = 'Longitude must be between 0 and 90 degrees.';
  }

  if (validator.isEmpty(data.longitude)) {
    errors.longitude = 'Longitude field is required.';
  }

  if (!validator.isFloat(data.speed, { min: 0, max: 9999 })) {
    errors.speed = 'Speed must be between 0 and 9999 meters';
  }


  if (!validator.isFloat(data.altitude, {  max: 20000 })) {
    errors.altitude = 'Altitude needs to be no more than 20000 meters.';
  }

  if (!validator.isFloat(data.heading, {  min: 0.00, max: 360.00 })) {
    errors.heading = 'Heading needs to be between 0 and 360 degrees.';
  }

  if (validator.isEmpty(data.uniqueident)) {
    errors.uniqueident = 'Uniqueident field is required.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
