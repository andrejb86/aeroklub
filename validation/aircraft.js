const validator = require('validator');
const isEmpty = require ('./is-empty');

module.exports = function validateAircraftInput(data) {
  let errors = {};

  data.organization = !isEmpty(data.organization) ? data.organization : '';
  data.title = !isEmpty(data.title) ? data.title : '';
  data.type = !isEmpty(data.type) ? data.type : '';
  data.class = !isEmpty(data.class) ? data.class : '';
  data.category = !isEmpty(data.category) ? data.category : '';
  data.registration = !isEmpty(data.registration) ? data.registration : '';
  data.pob = !isEmpty(data.pob) ? data.pob : '';

  if (validator.isEmpty(data.organization)) {
    errors.organization = 'Organization field is required.';
  }

  if (!validator.isLength(data.title, { min: 2, max: 30 })) {
    errors.title = 'Name must be between 2 and 30 characters';
  }

  if (validator.isEmpty(data.title)) {
    errors.title = 'Title field is required.';
  }

  if (!validator.isLength(data.type, { min: 2, max: 30 })) {
    errors.type = 'Type must be at least 2 characters.';
  }

  if (validator.isEmpty(data.type)) {
    errors.type = 'Type field is required.';
  }

  if (!validator.isLength(data.class, { min: 2, max: 30 })) {
    errors.class = 'Class must be at least 2 characters.';
  }

  if (validator.isEmpty(data.class)) {
    errors.class = 'Class field is required.';
  }

  if (!validator.isLength(data.category, { min: 2, max: 30 })) {
    errors.category = 'Category must be at least 2 characters.';
  }

  if (validator.isEmpty(data.category)) {
    errors.category = 'Category field is required.';
  }

  if (!validator.isLength(data.registration, { min: 2, max: 30 })) {
    errors.registration = 'Registration must be at least 2 characters.';
  }

  if (validator.isEmpty(data.registration)) {
    errors.registration = 'Registration field is required.';
  }

  if (!validator.isLength(data.pob, { min: 3, max: 5 })) {
    errors.pob = 'POB must be at least 3 characters.';
  }

  if (validator.isEmpty(data.pob)) {
    errors.pob = 'POB field is required.';
  }



  return {
    errors,
    isValid: isEmpty(errors)
  };
};
