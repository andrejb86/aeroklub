const validator = require('validator');
const isEmpty = require ('./is-empty');

module.exports = function validateOrganizationInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.registration = !isEmpty(data.registration) ? data.registration : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.description = !isEmpty(data.description) ? data.description : '';
  data.address = !isEmpty(data.address) ? data.address : '';

  if (!validator.isLength(data.name, { min: 3, max: 30 })) {
    errors.name = 'Name must be between 3 and 30 characters';
  }

  if (validator.isEmpty(data.name)) {
    errors.name = 'Name field is required.';
  }

  if (!validator.isLength(data.registration, { min: 3, max: 30 })) {
    errors.registration = 'Registration must be between 3 and 30 characters';
  }

  if (validator.isEmpty(data.registration)) {
    errors.registration = 'Registration field is required.';
  }

  if (!validator.isEmail(data.email)) {
    errors.email = 'Email is invalid.';
  }

  if (!validator.isLength(data.description, {  max: 200 })) {
    errors.description = 'Description needs to be no more than 200 characters.';
  }

  if (!validator.isLength(data.address, {  max: 100 })) {
    errors.address = 'Description needs to be no more than 100 characters.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
