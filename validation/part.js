const validator = require('validator');
const isEmpty = require ('./is-empty');

module.exports = function validateOrganizationInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.registration = !isEmpty(data.registration) ? data.registration : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.description = !isEmpty(data.description) ? data.description : '';
  data.address = !isEmpty(data.address) ? data.address : '';

  if (!validator.isLength(data.name, { min: 3, max: 50 })) {
    errors.name = 'Name must be between 3 and 50 characters';
  }

  if (validator.isEmpty(data.name)) {
    errors.name = 'Name field is required.';
  }

  if (validator.isEmpty(data.serial)) {
    errors.name = 'Serial number field is required.';
  }

  if (!validator.isLength(data.description, {  max: 200 })) {
    errors.description = 'Description needs to be no more than 200 characters.';
  }

  if (validator.isEmpty(data.hourly) && validator.isEmpty(data.expiryDate)) {
    errors.from = 'Expiry Date is required.';
  }

  if (validator.data.hourly === true && validator.isEmpty(data.hours)) {
    errors.from = 'Hours field is required.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
