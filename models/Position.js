const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const PositionSchema =  new Schema({
  pic: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  flight: {
    type: Schema.Types.ObjectId,
    ref: 'flights'
  },
  latitude: {
    type: Number,
    required: true
  },
  longitude: {
    type: Number,
    required: true
  },
  speed: {
    type: Number,
  },
  altitude: {
    type: Number,
  },
  heading: {
    type: Number,
  },
  uniqueident: {
    type: String,
    required: true
  },
  landing: {
    type: Boolean,
    default: false
  },
  takeoff: {
    type: Boolean,
    default: false
  },
  climb: {
    type: Boolean,
    default: false
  },
  descent: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  },
});

module.exports = Position = mongoose.model('positions', PositionSchema);
