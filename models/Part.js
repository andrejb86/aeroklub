const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const PartSchema =  new Schema({
 
  name: {
    type: String,
    required: true
  },
  serial: {
    type: String,
    required: true
  },
  description: {
    type: String,
  },
  hourly: {
    type: Boolean,
  },
  hours: {
    type: Number,
  },
  aircrafts: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'aircraft'
      }
    }
  ],
  date: {
    type: Date,
    default: Date.now
  },
  expiryDate: {
    type: Date,
  },
});

module.exports = Part = mongoose.model('parts', PartSchema);
