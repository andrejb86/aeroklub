const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const AircraftSchema =  new Schema({
  organization: {
    type: Schema.Types.ObjectId,
    ref: 'organization'
  },
  title: {
    type: String,
    required: true
  },

  type: {
    type: String,
    required: true
  },
  class: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  registration: {
    type: String,
    required: true
  },
  pob: {
    type: String,
    required: true
  },
  engine: {
    type: String,
  },
  landinggear: {
    type: String,
  },
  cockpit: {
    type: String,
  },
  complex: {
    type: Boolean,
    default: false
  },
  pressurized: {
    type: Boolean,
    default: false
  },
  highperformance: {
    type: Boolean,
    default: false
  },
  takeoffspeed: {
    type: String,
  },
  landingspeed: {
    type: String,
  },

  date: {
    type: Date,
    default: Date.now
  },
});

module.exports = Aircraft = mongoose.model('aircraft', AircraftSchema);
