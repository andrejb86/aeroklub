const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const StartListSchema =  new Schema({
  organization: {
    type: Schema.Types.ObjectId,
    ref: 'organizations'
  },
  flighttime: {
    type: Date,
    default: Date.now
  },
  type: {
    type: String,
    required: true
  },
  flights: [
    {
      flight: {
        type: Schema.Types.ObjectId,
        ref: 'flights'
      }
    }
  ],
  closed: {
    type: Boolean,
    default: false
  },
  weather: {
    type: String,
  },
  notes: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now
  },
});

module.exports = StartList = mongoose.model('startlists', StartListSchema);
