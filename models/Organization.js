const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const OrganizationSchema =  new Schema({
  name: {
    type: String,
    required: true
  },
  registration: {
    type: String,
    required: true
  },
  description: {
    type: String,
  },
  email: {
    type: String,
  },
  address: {
    type: String,
  },
  managers: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      }
    }
  ],
  date: {
    type: Date,
    default: Date.now
  },
});

module.exports = Organization = mongoose.model('organizations', OrganizationSchema);
