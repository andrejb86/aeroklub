const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const FlightSchema =  new Schema({
  startlist: {
    type: Schema.Types.ObjectId,
    ref: 'startlists'
  },
  aircraft: {
    type: Schema.Types.ObjectId,
    ref: 'aircraft'
  },
  pic: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  pilot2: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  assignment: {
    type: String,
    required: true
  },
  blockon: {
    type: Date,
    default: Date.now
  },
  blockoff: {
    type: Date,
    default: Date.now
  },
  takeoff: {
    type: Date,
    default: Date.now
  },
  release: {
    type: Date,
    default: Date.now
  },
  landing: {
    type: Date,
    default: Date.now
  },
  departure: {
    type: String,
    required: true
  },
  destination: {
    type: String,
    required: true
  },
  route: {
    type: Boolean,
    default: false
  },
  traning: {
    type: Boolean,
    default: false
  },
  schoolctivity: {
    type: Boolean,
    default: false
  },
  soaring: {
    type: Boolean,
    default: false
  },
  flighttime: {
    type: Date,
    default: Date.now
  },
  type: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  notes: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now
  },
});

module.exports = Flight = mongoose.model('flights', FlightSchema);
