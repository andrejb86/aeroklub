const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//Load validation
const validateProfileInput = require('../../validation/profile');
const validateExperienceInput = require('../../validation/experience');
const validateEducationInput = require('../../validation/education');

//Load profile model
const Profile = require('../../models/Profile');
//Load user model
const User = require('../../models/User');

// @route GET api/profile
// @desc Get Current User Profile
// @access Private
router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  const errors = {};

  Profile.findOne({ user: req.user.id })
  .populate('user', ['name' , 'avatar'])
  .then(profile => {
    if (!profile) {
      errors.noprofile = 'There is no profile for this user';
      return res.status(404).json(errors);
    }
    return res.json(profile);
  })
  .catch (err => res.status(404).json(err));
});

// @route GET api/profile/user/:user-id
// @desc Get Profile by User ID
// @access Public
router.get('/user/:user_id', (req, res) => {
  Profile.findOne({ user: req.params.user_id })
  .populate('user', ['name' , 'avatar'])
  .then(profile => {
    if (!profile) {
      errors.noprofile = 'There is no profile for this user';
      return res.status(404).json(errors);
    }
    return res.json(profile);
  })
  .catch (err => res.status(404).json({profile: 'There is no profile for this user.'}));
});

// @route GET api/profile/all
// @desc Get all profiles
// @access Public
router.get('/all', (req, res) => {
  Profile.find()
  .populate('user', ['name' , 'avatar'])
  .then(profiles => {
    if (!profiles) {
      errors.noprofile = 'There are no profiles.';
      return res.status(404).json(errors);
    }
    return res.json(profiles);
  })
  .catch (err => res.status(404).json({profiles: 'There are no profiles.'}));
});

// @route GET api/profile/handle/:handle
// @desc Get Profile by handle
// @access Public
router.get('/handle/:handle', (req, res) => {
  Profile.findOne({ handle: req.params.handle })
  .populate('user', ['name' , 'avatar'])
  .then(profile => {
    if (!profile) {
      errors.noprofile = 'There is no profile for this user';
      return res.status(404).json(errors);
    }
    return res.json(profile);
  })
  .catch (err => res.status(404).json(err));
});

// @route POST api/profile
// @desc Create Or Edit User Profile
// @access Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateProfileInput(req.body);

  //check validation
  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }

  //Get Fields
  const profileFields = {};
  profileFields.user = req.user.id;
  if (req.body.handle) profileFields.handle = req.body.handle;
  if (req.body.company) profileFields.company = req.body.company;
  if (req.body.website) profileFields.website = req.body.website;
  if (req.body.location) profileFields.location = req.body.location;
  if (req.body.bio) profileFields.bio = req.body.bio;
  if (req.body.status) profileFields.status = req.body.status;
  if (req.body.githubusername) profileFields.githubusername = req.body.githubusername;
  // Skills - Split into array
  if (typeof req.body.skills !== 'undefined') {
    profileFields.skills = req.body.skills.map(skill => skill.value);
  }

  console.log (profileFields.skills,req.body.skills);

  //Social
  profileFields.social = {};
  if (req.body.youtube) profileFields.social.youtube = req.body.youtube;
  if (req.body.facebook) profileFields.social.facebook = req.body.facebook;
  if (req.body.twitter) profileFields.social.twitter = req.body.twitter;
  if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
  if (req.body.instagram) profileFields.social.instagram = req.body.instagram;

  Profile.findOne({ user: req.user.id })
  .then(profile => {
    if (profile) {
      //Update
      Profile.findOneAndUpdate(
        { user: req.user.id},
        { $set: profileFields },
        { new: true }
      )
      .then(profile => res.json(profile))
      .catch(err => res.json(err));

    } else {
      //Create

      //Check if handle exists
      Profile.findOne({ handle: profileFields.handle }).then(profile => {
        if (profile) {
          errors.handle = 'That handle already exists';
          res.status(400).json(errors);
        }

        //Save profile
        new Profile(profileFields).save().then(profile => res.json(profile));
      });
    }
  })
  .catch (err => res.status(404).json(err));

});

// @route POST api/profile/experience
// @desc Add experience to profile
// @access Private
router.post('/experience', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateExperienceInput(req.body);

  //check validation
  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }

  Profile.findOne({ user: req.user.id })
  .then(profile => {
    const  NewExp = {
      title: req.body.title,
      company: req.body.company,
      location: req.body.location,
      from: req.body.from,
      to: req.body.to,
      current: req.body.current,
      description: req.body.description,
    };+

    //Add to exp array
    profile.experience.unshift(NewExp);
    profile.save().then(profile => res.json(profile));
  });
});

// @route POST api/profile/education
// @desc Add education to profile
// @access Private
router.post('/education', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateEducationInput(req.body);

  //check validation
  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }

  Profile.findOne({ user: req.user.id })
  .then(profile => {
    const  NewEdu = {
      school: req.body.school,
      degree: req.body.degree,
      fieldofstudy: req.body.fieldofstudy,
      from: req.body.from,
      to: req.body.to,
      current: req.body.current,
      description: req.body.description,
    };

    //Add to exp array
    profile.education.unshift(NewEdu);
    profile.save().then(profile => res.json(profile));
  });
});

// @route DELETE api/profile/experience/:exp_id
// @desc Delete experience in profile
// @access Private
router.delete('/experience/:exp_id', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateEducationInput(req.body);

  Profile.findOne({ user: req.user.id })
  .then(profile => {
    //Get remove index
    const removeIndex = profile.experience
      .map(item => item.id)
      .indexOf(req.params.exp_id);

    //Splice out of array
    profile.experience.splice(removeIndex, 1);

    //Save
    profile.save().then(profile => res.json(profile));
  })
  .catch(err => res.status(404).json(err));
});

// @route DELETE api/profile/education/:edu_id
// @desc Delete education in profile
// @access Private
router.delete('/education/:edu_id', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateEducationInput(req.body);

  Profile.findOne({ user: req.user.id })
  .then(profile => {
    //Get remove index
    const removeIndex = profile.education
      .map(item => item.id)
      .indexOf(req.params.exp_id);

    //Splice out of array
    profile.education.splice(removeIndex, 1);

    //Save
    profile.save().then(profile => res.json(profile));
  })
  .catch(err => res.status(404).json(err));
});


// @route DELETE api/profile
// @desc Delete user profile
// @access Private
router.delete('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  //const { errors, isValid } = validateEducationInput(req.body);

  Profile.findOneAndRemove({ user: req.user.id })
  .then(profile => {
    //find user to remove
    User.findOneAndRemove({ _id: req.user.id })
    .then(() => res.json({ success: true }))
  })
  .catch(err => res.status(404).json(err));
});

module.exports = router;
