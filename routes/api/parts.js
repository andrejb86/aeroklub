const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//parts model
const Part = require('../../models/Part');

//Post validation
const validatePartInput = require('../../validation/part');

// @route GET api/parts
// @desc Get all parts
// @access Public
router.get('/', (req, res) => {
  Part.find({}, {_id:false})
  .sort({date: -1})
  .then(parts => {
    if (!parts) {
      errors.nopart = 'There are no parts.';
      return res.status(404).json(errors);
    }
    return res.json(parts);
  })
  .catch (err => res.status(404).json({nopart: 'There are no parts.'}));
});

// @route GET api/parts/:id
// @desc Get part by id
// @access Public
router.get('/:id', (req, res) => {
  Part.findById(req.params.id)
  .then(parts => {
    if (!parts) {
      errors.nopart = 'There is no part.';
      return res.status(404).json(errors);
    }
    return res.json(parts);
  })
  .catch (err => res.status(404).json({nopart: 'There is no part.'}));
});

// @route POST api/parts
// @desc Add organiyation
// @access Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validatepartInput(req.body);

  //check access
  // if (!req.user.isAdmin) {
  //   //Return ant errors with 403 Status
  //   return res.status(403).json(errors);
  // }

  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }
  const newPart = {

    name: req.body.name,
    serial: req.body.serial,
    description: req.body.description,
    hourly: req.body.hourly,
    hours: req.body.hours,
    expiryDate: req.body.expiryDate,

  };

  part.findOne({ _id: req.body._id,})
  .then(part => {
      if (part) {
      //Update
      Part.findByIdAndUpdate(
         req.body._id,
        { $set: newPart },
        { new: false }
      )
      .then(profile => res.json(part))
      .catch(err => res.json(err));

    } else {
      //Create

      //Check if handle exists
      //Check if handle exists
      Part.findOne({ serial: req.body.serial }).then(part => {
        if (part) {
          errors.serial = 'That serial number already exists';
          res.status(400).json(errors);
        }
        else {
          //Save profile
          new Part(newPart).save().then(part => res.json(part));
        }
      })
    }
  }).catch (err => res.status(404).json(err));


});


module.exports = router;
