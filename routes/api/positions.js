const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//organizations model
const Position = require('../../models/Position');
const User = require('../../models/User');

//Post validation
const validatePositionInput = require('../../validation/position');

// @route GET api/positions
// @desc Get all positions
// @access Public
router.get('/', (req, res) => {
  Position.find()
  .sort({date: -1})
  .then(positions => {
    if (!positions) {
      errors.noposition = 'There are no positions.';
      return res.status(404).json(errors);
    }
    return res.json(positions);
  })
  .catch (err => res.status(404).json({post: 'There are no positions.'}));
});

// @route GET api/positions/:id
// @desc Get position by id
// @access Public
router.get('/id/:id', (req, res) => {
  Position.findById(req.params.id)
  .then(positions => {
    if (!positions) {
      errors.noposition = 'There is no position.';
      return res.status(404).json(errors);
    }
    return res.json(positions);
  })
  .catch (err => res.status(404).json({post: 'There is no position.'}));
});

// @route GET api/positions/flight/:flightId
// @desc Get position by Flight
// @access Public
router.get('/flight/:flightId', (req, res) => {
  Position.findOne({ flight: req.params.flightId,})
  .then(positions => {
    if (!positions) {
      errors.noposition = 'There is no position.';
      return res.status(404).json(errors);
    }
    return res.json(positions);
  })
  .catch (err => res.status(404).json({post: 'There is no position.'}));
});

// @route GET api/positions/uniqueident/:uniqueIdent
// @desc Get position by uniqueident
// @access Public
router.get('/uniqueident/:uniqueIdent', (req, res) => {
  Position.findOne({ uniqueident: req.params.uniqueIdent,})
  .then(positions => {
    if (!positions) {
      errors.noposition = 'There is no position.';
      return res.status(404).json(errors);
    }
    return res.json(positions);
  })
  .catch (err => res.status(404).json({post: 'There is no position.'}));
});



// @route POST api/position
// @desc Add position
// @access Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validatePositionInput(req.body);

  //check access
  // if (!req.user.isAdmin) {
  //   //Return ant errors with 403 Status
  //   return res.status(403).json(errors);
  // }

  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }
  const newPosition = {

    pic: req.body.pic,
    flight: req.body.flight,
    type: req.body.type,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    speed: req.body.speed,
    altitude: req.body.altitude,
    heading: req.body.heading,
    uniqueident: req.body.uniqueident,
    landing: req.body.landing,
    takeoff: req.body.takeoff,
    climb: req.body.climb,
    descent: req.body.descent,
    date: req.body.date,

  };

  Position.findOne({ _id: req.body._id,})
  .then(position => {
      if (position) {

        User.findById(position.pic)
        .then(user => {

          if(!user.id === req.user.id) {
            return res.status(403).json({ notauthorizededitposition: 'Not permitted to change this position' });
          } else {
            //Update
            Position.findByIdAndUpdate(
               req.body._id,
              { $set: newPosition },
              { new: false }
            )
            .then(profile => res.json(position))
            .catch(err => res.json(err));
        }
      })
    } else {
      //Create

      //Check if handle exists
      //Check if handle exists
      Position.findOne({ uniqueident: req.body.uniqueident }).then(position => {
        if (position) {
          errors.uniqueident = 'That uniqueident already exists';
          res.status(400).json(errors);
        }
        else {
          //Save profile
          new Position(newPosition).save().then(position => res.json(position));
        }

      })

    }

  }).catch (err => res.status(404).json(err));

});

// @route DELETE api/position
// @desc delete position
// @access Private
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
  Position.findOne({  _id: req.params.id})
  .then(position => {
    User.findById(position.pic)
    .then(user => {

      if(!user.id === req.user.id) {
        return res.status(403).json({ notauthorizededitposition: 'Not permitted to delete this position' });
      } else {

        //DELETE
        position.remove().then(() => res.json({ success: true }))
        .catch(err => res.status(404).json({ positionnotfound: 'No position found.' }));
      }



    })
  }).catch(err => res.status(404).json({ positionnotfound: 'No position found.' }));
});

module.exports = router;
