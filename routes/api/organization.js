const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//organizations model
const Organization = require('../../models/Organization');

//Post validation
const validateOrganizationInput = require('../../validation/organization');

// @route GET api/organizations
// @desc Get all organizations
// @access Public
router.get('/', (req, res) => {
  Organization.find({}, {_id:false})
  .sort({date: -1})
  .then(organizations => {
    if (!organizations) {
      errors.noorganization = 'There are no organizations.';
      return res.status(404).json(errors);
    }
    return res.json(organizations);
  })
  .catch (err => res.status(404).json({noorganization: 'There are no organizations.'}));
});

// @route GET api/organizations/:id
// @desc Get organization by id
// @access Public
router.get('/:id', (req, res) => {
  Organization.findById(req.params.id)
  .then(organizations => {
    if (!organizations) {
      errors.noorganization = 'There is no organization.';
      return res.status(404).json(errors);
    }
    return res.json(organizations);
  })
  .catch (err => res.status(404).json({noorganization: 'There is no organization.'}));
});

// @route POST api/organizations
// @desc Add organiyation
// @access Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateOrganizationInput(req.body);

  //check access
  // if (!req.user.isAdmin) {
  //   //Return ant errors with 403 Status
  //   return res.status(403).json(errors);
  // }

  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }
  const newOrganization = {

    name: req.body.name,
    registration: req.body.registration,
    description: req.body.description,
    email: req.body.email,
    address: req.body.address,

  };

  Organization.findOne({ _id: req.body._id,})
  .then(organization => {
      if (organization) {
      //Update
      Organization.findByIdAndUpdate(
         req.body._id,
        { $set: newOrganization },
        { new: false }
      )
      .then(profile => res.json(organization))
      .catch(err => res.json(err));

    } else {
      //Create

      //Check if handle exists
      //Check if handle exists
      Organization.findOne({ registration: req.body.registration }).then(organization => {
        if (organization) {
          errors.registration = 'That registration already exists';
          res.status(400).json(errors);
        }
        else {
          //Save profile
          new Organization(newOrganization).save().then(organization => res.json(organization));
        }

      })

    }

  }).catch (err => res.status(404).json(err));


});


module.exports = router;
