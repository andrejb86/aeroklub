const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//Aircraft model
const Aircraft = require('../../models/Aircraft');
const Organization = require('../../models/Organization');

//Post validation
const validateAircraftInput = require('../../validation/aircraft');

// @route GET api/aircraft
// @desc Get all aircraft
// @access Public
router.get('/', (req, res) => {
  Aircraft.find()
  .sort({date: -1})
  .then(aircraft => {
    if (!aircraft) {
      errors.nopaircraft = 'There are no aircraft.';
      return res.status(404).json(errors);
    }
    return res.json(aircraft);
  })
  .catch (err => res.status(404).json({nopaircraft: 'There are no aircraft.'}));
});

// @route GET api/aircraft/:id
// @desc Get aircraft by id
// @access Public
router.get('/:id', (req, res) => {
  Aircraft.findById(req.params.id)
  .then(aircraft => {
    if (!aircraft) {
      errors.nopaircraft = 'There is no aircraft.';
      return res.status(404).json(errors);
    }
    return res.json(aircraft);
  })
  .catch (err => res.status(404).json({nopaircraft: 'There is no aircraft.'}));
});

// @route POST api/aircraft
// @desc Add aircraft
// @access Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
  const { errors, isValid } = validateAircraftInput(req.body);

  //check access
  // if (!req.user.isAdmin) {
  //   //Return ant errors with 403 Status
  //   return res.status(403).json(errors);
  // }

  if (!isValid) {
    //Return ant errors with 400 Status
    return res.status(400).json(errors);
  }
  const newAircraft = {

    organization: req.body.organization,
    title: req.body.title,
    type: req.body.type,
    class: req.body.class,
    category: req.body.category,
    registration: req.body.registration,
    pob: req.body.pob,
    engine: req.body.engine,
    landinggear: req.body.landinggear,
    cockpit: req.body.cockpit,
    complex: req.body.complex,
    pressurized: req.body.pressurized,
    highperformance: req.body.highperformance,
    takeoffspeed: req.body.takeoffspeed,
    landingspeed: req.body.landingspeed,

  };

  Aircraft.findOne({ _id: req.body._id,})
  .then(aircraft => {
      if (aircraft) {

        Organization.findById(aircraft.organization)
        .then(organization => {

          if(!organization.managers.filter(manager => manager.user.toString() === req.user.id).length > 0) {
            return res.status(403).json({ notauthorizededitaircraft: 'Not permitted to change this aircraft' });
          } else {
            //Update
            Aircraft.findByIdAndUpdate(
               req.body._id,
              { $set: newAircraft },
              { new: false }
            )
            .then(profile => res.json(aircraft))
            .catch(err => res.json(err));
        }
      })
    } else {
      //Create
      Aircraft.findOne({ registration: req.body.registration }).then(aircraft => {
        if (aircraft) {
          errors.registration = 'That registration already exists';
          res.status(400).json(errors);
        }
        else {
          //Save profile
          new Aircraft(newAircraft).save().then(aircraft => res.json(aircraft));
        }

      })

    }

  }).catch (err => res.status(404).json(err));

});

// @route DELETE api/aircraft
// @desc delete aircraft
// @access Private
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
  Aircraft.findOne({  _id: req.params.id})
  .then(aircraft => {
    Organization.findById(aircraft.organization)
    .then(organization => {

      if(!organization.managers.filter(manager => manager.user.toString() === req.user.id).length > 0) {
        return res.status(403).json({ notauthorizedaircraft: 'Not permitted to delete this aircraft' });
      } else {

        //DELETE
        aircraft.remove().then(() => res.json({ success: true }))
        .catch(err => res.status(404).json({ aircraftnotfound: 'No aircraft found.' }));
      }



    })
  }).catch(err => res.status(404).json({ aircraftnotfound: 'No aircraft found.' }));
});
module.exports = router;
