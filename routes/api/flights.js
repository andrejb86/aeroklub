const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//organizations model
const Flight = require('../../models/Flight');

//Post validation
//const validateAircraftInput = require('../../validation/post');

// @route GET api/flights
// @desc Get all flights
// @access Public
router.get('/', (req, res) => {
  Flight.find({}, {_id:false})
  .sort({date: -1})
  .then(flights => {
    if (!flights) {
      errors.noflight = 'There are no flights.';
      return res.status(404).json(errors);
    }
    return res.json(flights);
  })
  .catch (err => res.status(404).json({post: 'There are no flights.'}));
});

// @route GET api/flights/:id
// @desc Get flight by id
// @access Public
router.get('/:id', (req, res) => {
  Flight.findById(req.params.id)
  .then(flights => {
    if (!flights) {
      errors.noflight = 'There is no flight.';
      return res.status(404).json(errors);
    }
    return res.json(flights);
  })
  .catch (err => res.status(404).json({post: 'There is no flight.'}));
});
