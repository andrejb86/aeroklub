const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//organizations model
const StartList = require('../../models/Flight');

//Post validation
//const validateAircraftInput = require('../../validation/post');

// @route GET api/startlists
// @desc Get all startlists
// @access Public
router.get('/', (req, res) => {
  StartList.find({}, {_id:false})
  .sort({date: -1})
  .then(startlists => {
    if (!startlists) {
      errors.nostartlists = 'There are no startlists.';
      return res.status(404).json(errors);
    }
    return res.json(startlists);
  })
  .catch (err => res.status(404).json({post: 'There are no startlists.'}));
});

// @route GET api/startlists/:id
// @desc Get startlist by id
// @access Public
router.get('/:id', (req, res) => {
  StartList.findById(req.params.id)
  .then(startlists => {
    if (!startlists) {
      errors.nostartlists = 'There is no startlist.';
      return res.status(404).json(errors);
    }
    return res.json(startlists);
  })
  .catch (err => res.status(404).json({post: 'There is no startlist.'}));
});
