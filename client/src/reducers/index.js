import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import profileReducer from './profileReducer';
import postReducer from './postReducer';
import snackbarReducer from './snackbarReducer';
import airCraftReducer from './aircraftReducer'

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  profile: profileReducer,
  post: postReducer,
  notification: snackbarReducer,
  aircraft: airCraftReducer
});
