/**
 * Snackbar reducer
 */
import * as types from "../actions/types";

const initialState = {
    notifications: []
};

export default function(state = initialState, action) {

    switch (action.type) {
        case types.ENQUEUE_SNACKBAR:
            return {
                ...state,
                notifications: [
                    ...state.notifications,
                    {
                        ...action.payload
                    }
                ]
            };
        case types.REMOVE_SNACKBAR:
            return {
                ...state,
                notifications: state.notifications.filter(
                    notification => notification.key !== action.key,
                ),
            };

        default:
            return state;
    }
    
}