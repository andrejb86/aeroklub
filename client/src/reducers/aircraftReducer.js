import {
    ADD_AIRCRAFT,
    GET_AIRCRAFTS,
    GET_AIRCRAFT,
    DELETE_AIRCRAFT,
    AIRCRAFT_LOADING
  } from "../actions/types";
  import isEmpty from "../validation/is-empty";
  
  const initialState = {
    aircrafts: [],
    aircraft: {},
    loading: false
  };
  
  export default function(state = initialState, action) {
    switch (action.type) {
      case AIRCRAFT_LOADING:
        return {
          ...state,
          loading: true
        };
      case GET_AIRCRAFTS:
        return {
          ...state,
          aircrafts: action.payload,
          loading: false
        };
      case GET_AIRCRAFT:
        return {
          ...state,
          aircraft: action.payload,
          loading: false
        };
      case ADD_AIRCRAFT:
        return {
          ...state,
          aircrafts: [action.payload, ...state.aircrafts]
        };
      case DELETE_AIRCRAFT:
        return {
          ...state,
          aircrafts: state.aircrafts.filter(aircraft => aircraft._id !== action.payload)
        };
      default:
        return state;
    }
  }
  