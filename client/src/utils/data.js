export const skillData = [
  { value :"PPL" , label: "PPL" },
  { value :"CPL" , label: "CPL" },
  { value :"ATPL" , label: "ATPL" },
  { value :"SEP" , label: "SEP" },
  { value :"MEP" , label: "MEP" },
  { value :"IR" , label: "IR" },
  { value :"FE" , label: "FE" },
  { value :"IFE" , label: "IFE" },
  { value :"NR" , label: "NR" },
  { value :"MCC" , label: "MCC" },
  { value :"LAPL" , label: "LAPL" },
]

export const displayErrors = (error) => {
  if (typeof error ==="object") {
    let errorString = "";
    for (let property in error) {
      errorString +=  error[property] + '; ';
    }
    return errorString
  }
  if (typeof error ==="string") 
    return error;
  return "";
}
