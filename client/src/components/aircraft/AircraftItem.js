import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';


/**
 * AircraftItem
 */
class AircraftItem extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {

    const { aircraft } = this.props;
    
    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-2">
          </div>
          <ul className="col-lg-6 col-md-4 col-8 list-group list-group-flush">
           <li className='list-group-item list-group-item-success' key={aircraft._id + '0'}>
            {aircraft.title}
            </li>
            <li className='list-group-item' key={aircraft._id + '1'}>
              type: {aircraft.type.toUpperCase()}
            </li>
            <li className='list-group-item' key={aircraft._id + '2'}>
              category: {aircraft.category.toUpperCase()}
            </li>
            <li className='list-group-item' key={aircraft._id + '3'}>
              class: {aircraft.class.toUpperCase()}
            </li>
            <li className='list-group-item' key={aircraft._id + '4'}>
              registration: {aircraft.registration.toUpperCase()}
            </li>

            <Link to={`/profile/${'profile.handle'}`} className="btn btn-info">
              View Aircraft
            </Link>
            
          </ul>
          <div className="col-md-4 d-none d-md-block">
            <h4>*TODO* Probaly parts come here</h4>
          </div>
        </div>
      </div>
    );
  }
}

AircraftItem.propTypes = {
  aircraft: PropTypes.object.isRequired
}

export default AircraftItem;