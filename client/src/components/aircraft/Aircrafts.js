import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../common/Spinner';
import { getAircrafts } from '../../actions/aircraftActions.js';
import AircraftItem from './AircraftItem';
/**
 * Profiles
 */

const SearchComponent = ({onChange}) => {
    return(
      <nav className="navbar navbar-light bg-light">
        <form className="form-inline">
          <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={onChange}/>
        </form>
      </nav>
    )
 }

 function compareSearchResults(plane, e){
   return (plane.title.toLowerCase().includes(e.target.value.toLowerCase()) 
          || plane.type.toLowerCase().includes(e.target.value.toLowerCase())
          || plane.class.toLowerCase().includes(e.target.value.toLowerCase())
          || plane.category.toLowerCase().includes(e.target.value.toLowerCase())
          || plane.registration.toLowerCase().includes(e.target.value.toLowerCase())
   )
  }
export class Aircrafts extends Component {
  
  constructor(props){
    super(props)

    this.state = {
      currentlyDisplayed: null
    }

    this.onInputChange = this.onInputChange.bind(this)
  }

  componentDidMount() {
    this.props.getAircrafts();
  }

  onInputChange(e){//when user filters, always filter current state from redux
    let newState = this.props.aircrafts.aircrafts.filter(plane => compareSearchResults(plane, e))

    this.setState({
      currentlyDisplayed: newState
    })
  }


  render() {
  
    const {aircrafts, loading } =  this.props.aircrafts; 
    const { currentlyDisplayed } = this.state;
   
    const planes = currentlyDisplayed !== null ? currentlyDisplayed : aircrafts;

    let aircraftItems;

    if (planes === null || loading) {
        aircraftItems = <Spinner />;

    } else {
      if (planes.length > 0){
        aircraftItems = planes.map(plane => (
          <AircraftItem key={plane._id} aircraft={plane}/>
        ))
      } else {
        aircraftItems = <h4>No aircrafts found.</h4>
      }
    }
    return (
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="row">
              <h1 className="display-4 offset-md-2 offset-sm-1 offset-xs-4 col-md-4 text-center">Aircrafts</h1>
               <SearchComponent onChange={this.onInputChange}/>
               </div>
              {aircraftItems}
            </div>
          </div>
        </div>
    );
  }
}
const mapStateToProps = state => ({
  aircrafts: state.aircraft
});

Aircrafts.propTypes = {
  aircrafts: PropTypes.object.isRequired,
  getAircrafts: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { getAircrafts })(Aircrafts);