import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../common/Spinner';
import { getProfiles } from '../../actions/profileActions';
import ProfileItem from './ProfileItem';
/**
 * Profiles
 */
export class Profiles extends Component {

  componentDidMount() {
    this.props.getProfiles();
  }
  render() {
    const { profiles, loading } =  this.props.profile;
    let profileItems;

    if (profiles === null || loading) {
      profileItems = <Spinner />;

    } else {
      if (profiles.length > 0){
        profileItems = profiles.map(profile => (
          <ProfileItem key={profile._id} profile={profile}/>
        ))
      } else {
        profileItems = <h4>No profiles found.</h4>
      }
    }
    return (
      <div className="profiles">{/*unused CSS-class, please check - DM*/}
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="display-4 text-center">Pilot Profiles</h1>
              <p className="lead text-center">Browse and connect with pilots.</p>
              {profileItems}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  profile: state.profile
});

Profiles.propTypes = {
  profile: PropTypes.object.isRequired,
  getProfiles: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { getProfiles })(Profiles);
