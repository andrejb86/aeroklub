import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';
import { clearCurrentProfile } from '../../actions/profileActions';

/**
 * Navbar
 */
class Navbar extends Component { // eslint-disable-line react/prefer-stateless-function

  onLogoutClick(e) {
    e.preventDefault();
    this.props.clearCurrentProfile();
    this.props.logoutUser();
  }

  componentDidMount() {
    const navBar = document.body.querySelector('nav.navbar');
    const collapsibleNav = navBar.querySelector('div.navbar-collapse');
    const btnToggle = navBar.querySelector('button.navbar-toggler');


    navBar.addEventListener('click', (e) => {
      if (e.target.classList.contains('dropdown-toggle') ) {
        return;
      }

      btnToggle.click();
    }, false);
  }

  render() {
    const { isAuthenticated, user } = this.props.auth;
    const authLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" data-toggle="dropdown-toggle" onSelect={() => true} to="/feed">Post Feed</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" data-toggle="dropdown-toggle" to="/dashboard">Dashboard</Link>
        </li>
        <li className="nav-item">
          <a
            href=""
            className="nav-link"
            data-toggle="dropdown"
            onClick={this.onLogoutClick.bind(this)}>
              <img src={user.avatar} alt={user.name} className='rounded-circle' style={{ width: '25px', 'marginRight':'5px' }}title="You must have Gravatar connected to your email to display an iamge"/>
              Logout
          </a>
        </li>
      </ul>
    );

    const guestLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" data-toggle="dropdown-toggle" to="/register">Sign Up</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" data-toggle="dropdown-toggle" to="/login">Login</Link>
        </li>
      </ul>
    );
    return (
  <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
    <div className="container">
      <Link className="navbar-brand" data-toggle="dropdown-toggle" to="/">Flight Manager</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-nav">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="mobile-nav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" data-toggle="dropdown-toggle" to="/profiles"> Dashboard
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" data-toggle="dropdown-toggle" to="/profiles"> Flights
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" data-toggle="dropdown-toggle" to="/aircraft"> Aircraft
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" data-toggle="dropdown-toggle" to="/profiles"> Organizations
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" data-toggle="dropdown-toggle" to="/profiles"> StartLists
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" data-toggle="dropdown-toggle" to="/profiles"> Pilots
            </Link>
          </li>
        </ul>
        {isAuthenticated ? authLinks : guestLinks}
      </div>
    </div>
  </nav>
    );
  }
}
Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentProfile })(Navbar);
