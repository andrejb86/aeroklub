import React, { Component } from 'react';

/**
 * Footer
 */
export class Footer extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <footer className="bg-dark text-white mt-5 p-4 text-center navbar-fixed-bottom">
        Copyright &copy; { new Date().getFullYear() } Flight Manager
      </footer>
    );
  }
}


export default Footer;
