import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from '../../validation/is-empty';
/**
 * ProfileAbout
 */
class ProfileAbout extends Component { // eslint-disable-line react/prefer-stateless-function

  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  render() {
    const { profile } = this.props;

    //Get First Name
    const firstName = profile.user.name.trim().split(' ')[0];
    const firstNameUp = this.capitalizeFirstLetter(firstName);
    //Skill list
    const skills = profile.skills.map((skill, index) => (
      <div key={index} className="p-3">
        <i className="fa fa-check" /> {skill}
      </div>
    ));
    return (
      <div className="row">
           <div className="col-md-12">
             <div className="card card-body bg-light mb-3">
               <h3 className="text-center text-info">{firstNameUp}'s Bio</h3>
               <p className="lead">
                 {isEmpty(profile.bio) ? <span>{firstNameUp} does not have a bio.</span> : (
                    <span>{profile.bio}</span>
                    )
                  }
               </p>
               <hr />
               <h3 className="text-center text-info">Skill Set</h3>
               <div className="row">
                 <div className="d-flex flex-wrap justify-content-center align-items-center">
                   {skills}
                 </div>
               </div>
             </div>
           </div>
         </div>
    );
  }
}

ProfileAbout.propTypes = {
  profile: PropTypes.object.isRequired
}

export default ProfileAbout;
