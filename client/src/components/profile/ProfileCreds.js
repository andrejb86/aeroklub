import React, { Component } from 'react';
import Moment from 'react-moment';
import isEmpty from '../../validation/is-empty';
/**
 * ProfileCreds
 */
class ProfileCreds extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {experience, education } = this.props;

    const expItems = experience.map(exp => (
      <li key={exp._id} className="list-group-item">
        <h4>{exp.company}</h4>
        <p><Moment format="MMMM YYYY">{exp.from}</Moment> -
          {isEmpty(exp.to) || exp.to === "" ? ( ' Now') : ( <Moment format="MMMM YYYY">{exp.to}</Moment>)}
        </p>
        <p>
          <strong>Position:</strong> {exp.title}
        </p>
        {isEmpty(exp.location) || exp.location === "" ? null : (  <p> <strong>Location:</strong> {exp.location}</p> )}

        {isEmpty(exp.description) || exp.description === "" ? null : (  <p> <strong>Description:</strong> {exp.description}</p> )}

      </li>
      ));

      const eduItems = education.map(edu => (
        <li key={edu._id} className="list-group-item">
          <h4>{edu.school}</h4>
          <p><Moment format="MMMM YYYY">{edu.from}</Moment> -
            {isEmpty(edu.to) || edu.to === "" ? ( ' Now') : ( <Moment format="MMMM YYYY">{edu.to}</Moment>)}
          </p>
          <p>
            <strong>Degree:</strong> {edu.degree}
          </p>
          {isEmpty(edu.fieldofstudy) || edu.fieldofstudy === "" ? null : (  <p> <strong>Field of study:</strong> {edu.fieldofstudy}</p> )}

          {isEmpty(edu.description) || edu.description === "" ? null : (  <p> <strong>Description:</strong> {edu.description}</p> )}

        </li>
        ));
    return (
      <div className="row">
        <div className="col-md-6">
          <h3 className="text-center text-info">Experience</h3>
          {expItems.length > 0 ? (
          <ul className="list-group">
            {expItems}
          </ul>
        ) : (
          <p className="text-center">No Experience Listed.</p>
        )}
        </div>
        <div className="col-md-6">
          <h3 className="text-center text-info">Education</h3>
          {eduItems.length > 0 ? (
              <ul className="list-group">
                {eduItems}
              </ul>
            ) : (
              <p className="text-center">No Education Listed.</p>
            )}
        </div>
      </div>
    );
  }
}


export default ProfileCreds;
