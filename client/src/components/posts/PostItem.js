import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { deletePost, addLike, removeLike } from "../../actions/postActions";
import { getProfileById } from "../../actions/profileActions";



/**
 * PostItem
 */
export class PostItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userHandle: "",
    };
  }
  onDeleteClick(id) {
    this.props.deletePost(id);
  }

  onLikeClick(id) {
    this.props.addLike(id);
  }

  onUnlikeClick(id) {
    this.props.removeLike(id);
  }

  findUserLike(likes) {
    const { auth } = this.props;
    if (likes.filter(like => like.user === auth.user.id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  getProfileById = (id) =>
    axios
      .get(`/api/profile/user/${id}`)
      .then(res => {
        const profile = res.data;
        this.setState({userHandle: profile.handle})
      })
      .catch(err => console.log(err));



  componentDidMount() {
    const { post } = this.props
    this.getProfileById(post.user);
  }

  render() {
    const { post, auth, showActions } = this.props;

    return (
     <div className="card card-body mb-3">
      <div className="row">
        <div className="col-md-2 mw-25 text-center">
          <Link to={`/profile/${this.state.userHandle}`} >
            <img
              className="rounded-circle d-none d-md-block"
              src={post.avatar}
              alt={post.name} />
          </Link>
          <br />
          <h5 className="text-center"><strong>{post.name}</strong></h5>
        </div>
        <div className="col-md-10">
          <p className="lead post-min-heigth">{post.text}</p>
          {showActions ? (
            <span>
            <button onClick={this.onLikeClick.bind(this, post._id)} type="button" className="btn btn-light mr-1">
              <i className={classnames('fas fa-thumbs-up',{
                'text-info' : this.findUserLike(post.likes)
              })}
              />
              <span className="badge badge-light">{post.likes.length}</span>
            </button>
            <button onClick={this.onUnlikeClick.bind(this, post._id)} type="button" className="btn btn-light mr-1">
              <i className="text-secondary fas fa-thumbs-down"></i>
            </button>
            <Link to={`/post/${post._id}`} className={classnames('btn mr-1',
                {'btn-info' : post.comments.length > 0},
                {'btn-light' : post.comments.length === 0}
              )}>
              Comments ({
                post.comments.length
              })
            </Link>
            { post.user === auth.user.id ? (
              <button onClick={this.onDeleteClick.bind(this, post._id)} type="button" className="btn btn-danger mr-1">
                <i className="fas fa-times" />
              </button>
              ) : null
            }
          </span>) : null }

        </div>
      </div>
    </div>
    );
  }
}
PostItem.defaultProps = {
  showActions: true
};

PostItem.propTypes = {
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  deletePost: PropTypes.func.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired,

}
const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(mapStateToProps, { deletePost, addLike, removeLike })(PostItem);
