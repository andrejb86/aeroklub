import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import Geosuggest from 'react-geosuggest';
import classnames from 'classnames';
import { addExperience } from '../../actions/profileActions';

/**
 * AddExperience
 */
class AddExperience extends Component {
  constructor() {
    super();
    this.state = {
      company: '',
      title: '',
      location: '',
      from: '',
      to: '',
      current: false,
      description: '',
      errors: {},
      disabled: false
    };

    this.onChange = this.onChange.bind(this);
    this.onCheck = this.onCheck.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onLocationChange = this.onLocationChange.bind(this);
    this.onLocationSelect = this.onLocationSelect.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({errors: nextProps.errors});
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  onLocationChange(location) {
    console.log(location);
    this.setState({ 'location': location });
  }

  onLocationSelect(location) {
    console.log(location.label);
    this.setState({ 'location': location.label });
  }

  onCheck(e) {
    this.setState({ 'current': !this.state.current });
    this.setState({ 'disabled': !this.state.disabled });
  }

  onSubmit(e) {
    e.preventDefault();

    const expData= {
      company: this.state.company,
      title: this.state.title,
      location: this.state.location,
      from: this.state.from,
      to: this.state.to,
      current: this.state.current,
      description: this.state.description,
    }

    this.props.addExperience(expData, this.props.history)
  }
  render() {
    const { errors } = this.state;

    return (
      <div className='add-experience'>
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light" >
                Go Back
              </Link>
              <h1 className="display-4 text-center">Add  Experience</h1>
              <p className="lead text-center">Add any job or position you have had in the past or current.</p>
              <small className="d-block pb-3">* = required fileds</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Company"
                  name="company"
                  value={this.state.company}
                  onChange={this.onChange}
                  error={errors.company}
                />
                <TextFieldGroup
                   placeholder="* Job Title"
                   name="title"
                   value={this.state.title}
                   onChange={this.onChange}
                   error={errors.title}
                />
                <Geosuggest
                  placeholder="* Location"
                  name="location"
                  inputClassName={classnames('form-control form-control-lg')}
                  initialValue={this.state.location}
                  onChange={this.onLocationChange}
                  onSuggestSelect={this.onLocationSelect}
                  error={errors.location}
                  autoComplete="off"
                  info="City or city & state suggested (eg. Boston, MA)"
                />
                <h6>* From Date</h6>
                <TextFieldGroup
                  name="from"
                  type="date"
                  placeholder="DD.MM.YYYY"
                  value={this.state.from}
                  onChange={this.onChange}
                  error={errors.from}
                />
                <h6>To Date</h6>
                <TextFieldGroup
                  name="to"
                  type="date"
                  value={this.state.to}
                  onChange={this.onChange}
                  error={errors.to}
                  disabled={this.state.disabled ? 'disabled' : '' }
                />
                <div className="form-check mb-4">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    name="current"
                    value={this.state.current}
                    checked={this.state.current}
                    onChange={this.onCheck}
                    id="current"
                  />
                  <label className="form-check-label" htmlFor="current">
                    Current job
                  </label>
                </div>
                <TextAreaFieldGroup
                  placeholder="Job Description"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  info="Tell us about the position"
                />
                <input type="submit" className="btn btn-info btn-block mt-4" />

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddExperience.propTypes = {
  profile: PropTypes.object.isRequired,
  addExperience: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  errors: state.errors,
  profile: state.profile
});

export default connect(mapStateToProps, { addExperience })(withRouter(AddExperience));
