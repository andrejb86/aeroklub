import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import SelectListGroup from '../common/SelectListGroup';
import InputGroup from '../common/InputGroup';
import { createProfile, getCurrentProfile } from '../../actions/profileActions';
import isEmpty from '../../validation/is-empty';
import Geosuggest from 'react-geosuggest';
import classnames from 'classnames';
import Select from 'react-select';
import { skillData } from '../../utils/data';

/**
 * EditProfile
 */
class EditProfile extends Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      displaySocialInputs: false,
      handle: '',
      company: '',
      website: '',
      location: '',
      bio: '',
      status: '',
      skills: '',
      githubusername: '',
      twitter: '',
      facebook: '',
      linkedin: '',
      youtube: '',
      instagram: '',
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onLocationChange = this.onLocationChange.bind(this);
    this.onLocationSelect = this.onLocationSelect.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({errors: nextProps.errors});
    }

    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      // Bring skills array back select box
      const skillsArray = profile.skills.map(skill => {
        let skillObject = { value: skill, label: skill};
        return  skillObject;
      });
      console.log (skillsArray, skillData);

      //If profile filed doesn't exist, make emty String
      profile.company = !isEmpty(profile.company) ? profile.company : '';
      profile.website = !isEmpty(profile.website) ? profile.website : '';
      profile.location = !isEmpty(profile.location) ? profile.location : '';
      profile.githubusername = !isEmpty(profile.githubusername) ? profile.githubusername : '';
      profile.bio = !isEmpty(profile.bio) ? profile.bio : '';
      profile.social = !isEmpty(profile.social) ? profile.social : {};
      profile.twitter = !isEmpty(profile.social.twitter) ? profile.social.twitter : '';
      profile.facebook = !isEmpty(profile.social.facebook) ? profile.social.facebook : '';
      profile.linkedin = !isEmpty(profile.social.linkedin) ? profile.social.linkedin : '';
      profile.youtube = !isEmpty(profile.social.youtube) ? profile.social.youtube : '';
      profile.instagram = !isEmpty(profile.social.instagram) ? profile.social.instagram : '';

      //Set componenet filed set
      this.setState({ handle: profile.handle });
      this.setState({ status: profile.status });
      this.setState({ company: profile.company });
      this.setState({ website: profile.website });
      this.setState({ githubusername: profile.githubusername });
      this.setState({ bio: profile.bio });
      this.setState({ skills: skillsArray});
      this.setState({ location: profile.location });
      this.setState({ twitter: profile.twitter });
      this.setState({ facebook: profile.facebook });
      this.setState({ linkedin: profile.linkedin });
      this.setState({ youtube: profile.youtube });
      this.setState({ instagram: profile.instagram });

    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  //Function for handling skill selection
  handleChange = selectedOptions => {
   this.setState({ skills: selectedOptions });
  };

  onLocationChange(location) {
    console.log(location);
    this.setState({ 'location': location });
  }

  onLocationSelect(location) {
    console.log(location.label);
    this.setState({ 'location': location.label });
  }

  onSubmit(e) {
    e.preventDefault();
    const profileData = {
      handle: this.state.handle,
      company: this.state.company,
      website: this.state.website,
      location: this.state.location,
      status: this.state.status,
      skills: this.state.skills,
      githubusername: this.state.githubusername,
      bio: this.state.bio,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      linkedin: this.state.linkedin,
      youtube: this.state.youtube,
      instagram: this.state.instagram
    };

    this.props.createProfile(profileData, this.props.history);
    };

  render() {
    const { errors, displaySocialInputs } = this.state;

    let socialInputs;
    if (displaySocialInputs) {
     socialInputs = (
            <div>
              <InputGroup
                placeholder="Twitter Profile URL"
                name="twitter"
                icon="fab fa-twitter"
                value={this.state.twitter}
                onChange={this.onChange}
                error={errors.twitter}
              />

              <InputGroup
                placeholder="Facebook Page URL"
                name="facebook"
                icon="fab fa-facebook"
                value={this.state.facebook}
                onChange={this.onChange}
                error={errors.facebook}
              />

              <InputGroup
                placeholder="Linkedin Profile URL"
                name="linkedin"
                icon="fab fa-linkedin"
                value={this.state.linkedin}
                onChange={this.onChange}
                error={errors.linkedin}
              />

              <InputGroup
                placeholder="YouTube Channel URL"
                name="youtube"
                icon="fab fa-youtube"
                value={this.state.youtube}
                onChange={this.onChange}
                error={errors.youtube}
              />

              <InputGroup
                placeholder="Instagram Page URL"
                name="instagram"
                icon="fab fa-instagram"
                value={this.state.instagram}
                onChange={this.onChange}
                error={errors.instagram}
              />
          </div>
      );
    }
    //Selet options for status
    const options = [
      { label: '* Select Professional Status', value: 0 },
      { label: 'Student Pilot', value: 'Student Pilot' },
      { label: 'Private Pilot', value: 'Private Pilot' },
      { label: 'Professional Pilot', value: 'Professional Pilot' },
      { label: 'Manager', value: 'Manager' },
      { label: 'Flight Instructor', value: 'Flight Instructor' },
    ];
    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light" >
                Go Back
              </Link>
              <h1 className="display-4 text-center">Edit  Profile</h1>
              <small className="d-block pb-3">* = required fileds</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                   placeholder="Profile Handle"
                   name="handle"
                   value={this.state.handle}
                   onChange={this.onChange}
                   error={errors.handle}
                   disabled={'disabled'}
                   info="A unique handle for your profile URL. Your full name, company name, nickname, etc"
                />
                <SelectListGroup
                   placeholder="Status"
                   name="status"
                   value={this.state.status}
                   options = {options}
                   onChange={this.onChange}
                   error={errors.status}
                   info="Give us an idea of where you are at in your career"
                />
                <TextFieldGroup
                  placeholder="Company"
                  name="company"
                  value={this.state.company}
                  onChange={this.onChange}
                  error={errors.company}
                  info="Could be your own company or one you work for"
                />
                <TextFieldGroup
                  placeholder="Website"
                  name="website"
                  value={this.state.website}
                  onChange={this.onChange}
                  error={errors.website}
                  info="Could be your own website or a company one"
                />
                <Geosuggest
                  placeholder="Location"
                  name="location"
                  inputClassName={classnames('form-control form-control-lg')}
                  initialValue={this.state.location}
                  onChange={this.onLocationChange}
                  onSuggestSelect={this.onLocationSelect}
                  error={errors.location}
                  autoComplete="off"
                  info="City or city & state suggested (eg. Boston, MA)"
                />
                <div className="form-group">
                  <Select
                    value={this.state.skills}
                    isMulti
                    name="skills"
                    options={skillData}
                    onChange={(...args) => this.handleChange(...args)}
                    className={classnames('basic-multi-select', {
                      'is-invalid' : errors.skills
                    })}
                    classNamePrefix=""
                    placeholder="* Skills"
                    info=""
                  />
                  <small className="form-text text-muted">Start typing and select your skills (eg. PPL, IR, NR</small>
                  {  errors.skills ? (<div className="text-danger small">{errors.skills}</div>) : null }
                </div>
                <TextFieldGroup
                  placeholder="Github Username"
                  name="githubusername"
                  value={this.state.githubusername}
                  onChange={this.onChange}
                  error={errors.githubusername}
                  info="If you want your latest repos and a Github link, include your username"
                />
                <TextAreaFieldGroup
                  placeholder="Short Bio"
                  name="bio"
                  value={this.state.bio}
                  onChange={this.onChange}
                  error={errors.bio}
                  info="Tell us a little about yourself"
                />
                <div className="mb-3">
                  <button
                    type="button"
                    className="btn-btn-light"
                    onClick={() => {
                      this.setState(prevState => ({
                        displaySocialInputs: !prevState.displaySocialInputs
                      }))
                      }}
                  >
                    Add Social Network Links
                  </button>
                </div>
                {socialInputs}
                <input type="submit" className="btn btn-info btn-block mt-4" />

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

EditProfile.propTypes = {
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  // auth: state.auth,
  errors: state.errors,
  profile: state.profile
});

export default connect(mapStateToProps, { createProfile, getCurrentProfile })(withRouter(EditProfile));
