import {
    ENQUEUE_SNACKBAR,
    REMOVE_SNACKBAR,
  } from './types';

  export const doEnqueueSnackBar = (msg, variant) => {
    
    const notificacion = {
        message: msg,
        options: {
            variant: variant,
        },
    }
    return {
        type: ENQUEUE_SNACKBAR,
        payload: {
            key: new Date().getTime() + Math.random(),
            ...notificacion
        }
      };
}