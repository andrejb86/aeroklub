import axios from 'axios';
import {
  GET_AIRCRAFT,
  GET_AIRCRAFTS,
  ADD_AIRCRAFT,
  DELETE_AIRCRAFT,
  AIRCRAFT_LOADING,
  GET_ERRORS,
  CLEAR_ERRORS,
} from './types';
import { doEnqueueSnackBar } from './notificationActions';
import {displayErrors} from '../utils/data';

//Add Aircraft
export const addAircraft = aircraftData => dispatch => {
  dispatch(clearErrors());
  axios
    .Aircraft('/api/aircraft', aircraftData )
    .then( res => {
      dispatch({
        type: ADD_AIRCRAFT,
        payload: res.data
      });
      dispatch(doEnqueueSnackBar("aircraft updated succesfully", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Aircraft error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Get Aircrafts
export const getAircrafts = () => dispatch => {
  dispatch(setAircraftLoading());
  axios
    .get('/api/aircraft')
    .then( res => {
      dispatch({
        type: GET_AIRCRAFTS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_AIRCRAFTS,
        payload: null
      });
      dispatch(doEnqueueSnackBar("Aircraft error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Get Aircraft
export const getAircraft = (id) => dispatch => {
  dispatch(setAircraftLoading());
  axios
    .get(`/api/aircraft/${id}`)
    .then( res => {
      dispatch({
        type: GET_AIRCRAFT,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_AIRCRAFT,
        payload: null
      });
      dispatch(doEnqueueSnackBar("Aircraft error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Delete Aircraft
export const deleteAircraft = id => dispatch => {
  axios
    .delete(`/api/aircraft/${id}`)
    .then( res => {
      dispatch({
        type: DELETE_AIRCRAFT,
        payload: id
      });
      dispatch(doEnqueueSnackBar("aircraft deleted succesfully", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Aircraft error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Set loading stateless
export const setAircraftLoading = () => {
  return {
    type: AIRCRAFT_LOADING
  }
}

//Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  }
}
