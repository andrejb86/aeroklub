import axios from 'axios';
import { GET_ERRORS, SET_CURRENT_USER } from './types';
import setAuthToken from '../utils/setAuthToken';
import { doEnqueueSnackBar } from './notificationActions';
import {displayErrors} from '../utils/data';
import jwt_decode from 'jwt-decode';

//Register user
export const registerUser = (userData, history) => dispatch => {
  axios
    .post('/api/users/register', userData)
    .then(res => {
      history.push('/login')
      dispatch(doEnqueueSnackBar("Registration successful.", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Registration error: "+displayErrors(err.response.data), 'error'));
    }
  );
}
//Logib - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post('/api/users/login', userData)
    .then(res => {
      //Save to local storage
      const { token } = res.data;
      //Set token to local storage
      localStorage.setItem('jwtToken', token);
      // Set token to auth header
      setAuthToken(token);
      //Decode token to get user userData
      const decoded =  jwt_decode(token);
      // Set current users dispatch(setCurrentUser(decoded));
      dispatch(setCurrentUser(decoded));
      dispatch(doEnqueueSnackBar("Login successful.", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Login error: "+displayErrors(err.response.data), 'error'));
    });
};

//set logged in users
export const setCurrentUser = (decoded) => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  }
}

//Log user out

export const logoutUser = () => dispatch => {
  //Remove token from localStorage
  localStorage.removeItem('jwtToken');
  //Remove auth header for future requests
  setAuthToken(false);
  //Set current user to {} which will set  isAuthenticated to false
  dispatch(setCurrentUser({}));
  
}
