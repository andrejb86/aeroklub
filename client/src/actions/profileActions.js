import axios from 'axios';
import {
  GET_PROFILE,
  GET_PROFILES,
  PROFILE_LOADING,
  GET_ERRORS,
  CLEAR_ERRORS,
  CLEAR_CURRENT_PROFILE,
  SET_CURRENT_USER
} from './types';
import { doEnqueueSnackBar } from './notificationActions';
import {displayErrors} from '../utils/data';

//Get current profile
export const getCurrentProfile = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get('/api/profile')
    .then(res => {
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_PROFILE,
        payload: {}
      });
      dispatch(doEnqueueSnackBar("Profile error: "+displayErrors(err.response.data), 'error'));
    }
  );
};

//Get all profiles
export const getProfiles = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get('/api/profile/all')
    .then(res => {
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      });
      //dispatch(doEnqueueSnackBar("Profiles loaded succesfully", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_PROFILES,
        payload: null
      });
      doEnqueueSnackBar("Profile fetch error", 'error');
    }
  );
};

//Get  profile by handle
export const getProfileByHandle = (handle) => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get(`/api/profile/handle/${handle}`)
    .then(res => {
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
      //dispatch(doEnqueueSnackBar("Profile loaded succesfully", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_PROFILES,
        payload: null
      });
      doEnqueueSnackBar("Profile fetch error", 'error');
    }
  );
};


//Create profile
export const createProfile = (profileData, history) => dispatch => {
  dispatch(clearErrors());
  axios
    .post('/api/profile', profileData )
    .then(res => {
      history.push('/dashboard')
      dispatch(doEnqueueSnackBar("Profile updated succesfully", 'success'));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Profile error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Add Experience
export const addExperience = (expData, history) => dispatch => {
  axios
    .post('/api/profile/experience', expData )
    .then(res => history.push('/dashboard'))
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Profile error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Add Experience
export const addEducation = (eduData, history) => dispatch => {
  axios
    .post('/api/profile/education', eduData )
    .then(res => history.push('/dashboard'))
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Profile error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Delete Experience
export const deleteExperience = (id) => dispatch => {
  axios
    .delete(`/api/profile/experience/${id}`)
    .then(res => {
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Profile error: "+displayErrors(err.response.data), 'error'));
    }
  );
}

//Delete Experience
export const deleteEducation = (id) => dispatch => {
  axios
    .delete(`/api/profile/education/${id}`)
    .then(res => {
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
      dispatch(doEnqueueSnackBar("Profile error: "+displayErrors(err.response.data), 'error'));
    }
  );
}
//Delete profile
export const deleteAccount = () => dispatch => {
  axios
    .delete('/api/profile')
    .then(res => dispatch({
        type: SET_CURRENT_USER,
        payload: {}
      })
    )
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    }
  );
}

//Profie loading
export const setProfileLoading = () => {
  return {
    type: PROFILE_LOADING
  }
}

export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  }
}

//Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  }
}
