const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt-nodejs');
const cors = require ('cors');
const passport = require('passport');
const path = require('path');

const users = require('./routes/api/users');
const profile = require('./routes/api/profile');
const posts = require('./routes/api/posts');
const aircraft = require('./routes/api/aircraft');
const organizations = require('./routes/api/organization');
const positions = require('./routes/api/positions');

const app = express();

// DB config

const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true } )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err))

//Passport middleware
app.use(passport.initialize());

//Passport config
require('./config/passport')(passport);

//Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

//Use routes
app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/posts', posts);
app.use('/api/aircraft', aircraft);
app.use('/api/organizations', organizations);
app.use('/api/positions', positions);

// Server static assets id in production
if (process.env.NODE_ENV === 'production') {
  //Set static folder
  app.use(express.static('client/build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server running on port ${port}`));